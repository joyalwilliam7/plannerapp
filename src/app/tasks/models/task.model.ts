export interface Task {
    id: string;
    title: string;
    done: boolean;
    resume: string;
    status: string;
    hour: number;
    color: string;
    time: string;
    date: string;
}
