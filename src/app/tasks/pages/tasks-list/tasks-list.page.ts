import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Task } from '../../models/task.model';
import { TasksService } from '../../services/tasks.service';
import { NavController } from '@ionic/angular';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { NotifierService } from 'angular-notifier';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.page.html',
  styleUrls: ['./tasks-list.page.scss'],
})
export class TasksListPage {

  tasks$: Observable<Task[]>
  taskssch: any[] = [
    {
      hour: 1,
      colorHex: '#F9BE7C',
      title: 'Project Research',
      description: undefined
    },
    {
      hour: 5,
      colorHex: '#e77480',
      title: 'Call',
      description: 'Discuss with the customer of the medical apllication the references that he send.'
    },
    {
      hour: 8,
      colorHex: '#7494e7',
      title: 'Make tasks for the medical app',
      description: undefined
    },
    {
      hour: 1,
      colorHex: '#459ea1',
      title: 'Design meeting',
      description: 'Discuss with designer new tasks for the medical appp. Tell how the call went.'
    },

  ];
  constructor( private navCtrl:NavController,
               private tasksService: TasksService,
               private overlayService: OverlayService,
               private notifier: NotifierService) { }

   async ionViewDidEnter(): Promise<void> {
    const loading = await this.overlayService.loading();
    this.tasks$ = this.tasksService.getAll();
    this.tasks$.pipe(take(1)).subscribe( tasks => loading.dismiss());
  }

  public showNotification( type: string, message: string ): void {
    this.notifier.notify( type, message );
  }

  onUpdate(task: Task): void {
    this.navCtrl.navigateForward(`/tasks-list/edit/${task.id}`)
  }

  async onDelete(task:Task):Promise<void> {
    await this.overlayService.alert({
      message: `Do you want to delete the task "${task.title}"?`,
      buttons: [
        {
          text: 'Yes',
          handler: async ()=> {
            await this.tasksService.delete(task);
            await this.overlayService.toast({
              message: `Assignment "${task.title}" deleted!`
            })
          }
        },
        'No'
      ]
    })
  }

  async onDone(task: Task): Promise<void> {
    const taskToUpdate = { ...task, done:!task.done };
    await this.tasksService.update(taskToUpdate);
    this.showNotification(
      'success',
      `Activity "${task.title}" ${taskToUpdate.done ? 'was completed' : 'has been updated'}`
    );
  }

}
