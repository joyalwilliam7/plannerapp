// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: 'AIzaSyCwMMqxf02MC4sqYawuRv7AQZh_kPVDoec',
  authDomain: 'book-67d6d.firebaseapp.com',
  databaseURL: 'https://book-67d6d.firebaseio.com',
  projectId: 'book-67d6d',
  storageBucket: 'book-67d6d.appspot.com',
  messagingSenderId: '207033650112',
  appId: '1:207033650112:web:a312dd930af19313111625'
  }
};
